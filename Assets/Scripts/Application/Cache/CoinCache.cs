﻿using System;
using UniRx;
using UnityEngine;

public sealed class CoinCache : MonoBehaviour
{
    // Mutable Composite Field
    readonly ReactiveProperty<int> count = new ReactiveProperty<int>();

    // Value
    public int Count => count.Value;

    // Event
    public IObservable<int> CountAsObservable => count;

    // Initialize View
    void Awake()
    {
        count.AddTo(this);
    }

    // Function
    public void SetCount(int count)
    {
        this.count.Value = count;
    }

    public void AddCount(int count)
    {
        this.count.Value += count;
    }
}
