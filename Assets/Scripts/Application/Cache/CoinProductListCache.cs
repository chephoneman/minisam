﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

public class CoinProductListCache : MonoBehaviour
{
    // Mutable Composite Field
    readonly ReactiveProperty<Product[]> products = new ReactiveProperty<Product[]>();

    // Mutable State Field
    bool hasProducts;

    // Value
    public Product[] Products => products.Value;

    // Event
    public IObservable<Product[]> ProductsAsObservable => products.Where(_ => hasProducts);

    // Function
    public void UpdateContents(IEnumerable<Product> products)
    {
        this.hasProducts = true;
        this.products.Value = products.ToArray();
    }
}
