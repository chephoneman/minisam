﻿using UniRx;
using Zenject;

public class CoinCountState : ViewState
{
    // Cache Injection
    [Inject] CoinCache coinCache = default;

    // Initialize View
    void Awake()
    {
        coinCache.CountAsObservable
            .Subscribe(UpdateValue)
            .AddTo(this);
    }
}
