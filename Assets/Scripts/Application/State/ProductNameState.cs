﻿using UniRx;
using Zenject;

public class ProductNameState : ViewState
{
    // State Injection
    [Inject] ProductState productState = default;

    // Initialize View
    void Start()
    {
        productState.ValueAsObservable
            .Select(value => value as Product)
            .Select(product => product.Name)
            .Subscribe(UpdateValue)
            .AddTo(this);
    }
}
