﻿using UniRx;
using Zenject;

public class CoinProductListState : ViewState
{
    // Cache Injection
    [Inject] CoinProductListCache coinProductListCache = default;

    // Initialize View
    void Awake()
    {
        coinProductListCache.ProductsAsObservable
            .Subscribe(UpdateValue)
            .AddTo(this);
    }
}
