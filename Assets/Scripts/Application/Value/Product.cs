﻿public sealed class Product
{
    // Value
    public string Id { get; }
    public string Name { get; }
    public int Amount { get; }
    public int Price { get; }

    // Initialize Value
    public Product(string id, string name, int amount, int price)
    {
        Id = id;
        Name = name;
        Amount = amount;
        Price = price;
    }
}
