﻿using Zenject;

public class AddCoinAction : ViewAction
{
    // Cache Injection
    [Inject] CoinCache coinCache = default;

    // Presenter Injection
    [Inject] MessagePresenter messagePresenter = default;

    // Inherit Method
    protected override void OnExecute(object[] arguments)
    {
        var addValue = (int)arguments[0];
        coinCache.AddCount(addValue);
        messagePresenter.Show($"add coin :{addValue}");
    }
}
